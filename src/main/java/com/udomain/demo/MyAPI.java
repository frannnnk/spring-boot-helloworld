package com.udomain.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MyAPI {

    @Autowired
    private BlogRepository repository;

    @RequestMapping(value = "/blog",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    public List<Blog> getBlog(){

        return repository.findAll();
    }

    @RequestMapping(value = "/blog",
                    consumes = {"application/json"},
                    produces = {"application/json"},
                    method = RequestMethod.POST)
    public void saveBlog(Blog blog){

        repository.save(blog);
    }


    @RequestMapping(value = "/blog/{id}",
                    produces = {"application/json"},
                    method = RequestMethod.POST)
    public void updateBlogs(@RequestBody Blog blog, @PathVariable("id") Integer id){

        Blog oldBlog = repository.findById(id).get();
        if (oldBlog != null) {
            oldBlog.setTitle(blog.getTitle());
            oldBlog.setContent(blog.getContent());
            repository.save(oldBlog);
        }
    }




    @RequestMapping(value = "/blog/{title}",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    public List<Blog> getBlogLaLaLasssss(@PathVariable("title")  String title){

        return repository.findByTitle(title);
    }


    @RequestMapping(value = "/blog/{id}",
                    produces = {"application/json"},
                    method = RequestMethod.DELETE)
    public void deleteBlogs(@PathVariable("id") Integer id){
        repository.deleteById(id);
    }


}
