package com.udomain.demo;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BlogRepository extends JpaRepository<Blog, Integer> {

    List<Blog> findByTitle(String title);

    List<Blog> findByTitleNotContains(String something);
}
